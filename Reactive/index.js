/*
let clickSource = Rx.Observable.fromEvent(document.querySelector('button'), 'click');

clickSource.subscribe(console.log);*/

let observable = Rx.Observable.create(function (observer) {
    observer.next(1);
    observer.next(2);
    observer.next(3);
    setTimeout(() => {
      observer.next(4);
      observer.complete();
    }, 1000);
  });