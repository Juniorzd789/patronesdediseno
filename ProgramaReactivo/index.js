let clickSource = Rx.Observable
.fromEvent(document.querySelector('button'), 'click');

clickSource.bufferTime(1000)
  .map(list =>list.length)
  .filter(contador => contador>0)
  .subscribe(conteo =>{
  document.querySelector("#counter").innerHTML = conteo;
});

//clickSource.subscribe(console.log);